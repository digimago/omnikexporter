FROM golang:latest AS build
LABEL org.opencontainers.image.authors="svisser@digimago.nl"

WORKDIR /go/src
COPY . .
RUN go build -o omniklistener
CMD ./omniklistener 6666 9110
