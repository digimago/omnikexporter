package main

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var previousValue int = 0

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	if len(os.Args[1:]) < 2 {
		log.Fatal().Msg("Invalid number of arguments passed. Use 'omniklistener <CONN_PORT> <PROM_PORT>'.")
		os.Exit(1)
	}

	var CONN_PORT = string(os.Args[1])
	var PROM_PORT = string(os.Args[2])

	myRouter := mux.NewRouter()
	myRouter.Path("/metrics").Handler(promhttp.Handler())

	srv := &http.Server{
		Addr:              ":" + PROM_PORT,
		ReadHeaderTimeout: 15 * time.Second,
		ReadTimeout:       15 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       30 * time.Second,
		Handler:           myRouter,
	}

	go func() {
		log.Info().Msg("Now accepting connections on /metrics")
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal().Err(err).Msg("could not serve /metrics endpoint")
		}
	}()

	ln, err := net.Listen("tcp", "0.0.0.0:"+CONN_PORT)
	if err != nil {
		log.Fatal().Err(err).Msg("could not bind on tcp port")
	} else {
		log.Info().Msg(fmt.Sprintf("Now accepting connections on port %s", CONN_PORT))
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal().Msg("Failed to accept incoming connections")
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	log.Info().Msg("now handling a new connection")
	netData := make([]byte, 128)
	_, err := io.ReadFull(conn, netData)
	if err != nil {
		log.Error().Err(err).Msg("Error reading from socket")
	}
	outbytes := make([]byte, 1024)
	handleData(netData)
	_, err = conn.Write(outbytes)
	if err != nil {
		log.Error().Err(err).Msg("Error writing to socket")
	}
}

var (
	mDailyOutput = promauto.NewCounter(prometheus.CounterOpts{
		Name: "omniksol_daily_energy_delivery_total",
		Help: "The total of today's delivered power",
	})
)
var (
	mTemperature = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "omniksol_pv_inverter_temperature_centigrade",
		Help: "Current internal temperature of PV inverter",
	})
)
var (
	mPVVoltage = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "omniksol_pv_voltage",
		Help: "Current voltage of PV string",
	})
)
var (
	mPVCurrent = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "omniksol_pv_current",
		Help: "Current supplied by PV panels",
	})
)
var (
	mACPower = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "omniksol_pv_ac_power",
		Help: "Current power delivered to AC grid",
	})
)

func handleData(data []byte) {
	newData := Pvdata{
		daily_output: (((int(data[69]) * 256) + int(data[70])) * 10),
		temperature:  (((int(data[31]) * 256) + int(data[32])) / 10),
		pv_voltage:   (((int(data[33]) * 256) + int(data[34])) / 10),
		pv_current:   (((int(data[39]) * 256) + int(data[40])) / 10),
		ac_power:     ((int(data[59]) * 256) + int(data[60])),
	}
	var latestValue int = newData.daily_output
	var delta int = latestValue - previousValue

	mDailyOutput.Add(float64(delta))
	mTemperature.Set(float64(newData.temperature))
	mPVVoltage.Set(float64(newData.pv_voltage))
	mPVCurrent.Set(float64(newData.pv_current))
	mACPower.Set(float64(newData.ac_power))

	log.Info().Msg(fmt.Sprintf("newData: %v", newData))

}

type Pvdata struct {
	daily_output int
	temperature  int
	pv_voltage   int
	pv_current   int
	ac_power     int
}
